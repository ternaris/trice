with import <nixpkgs> {};

let
    python = python38;

in

stdenv.mkDerivation rec {
  name = baseNameOf (builtins.getEnv "PWD");
  shellHook = ''
    set -e
    export VENV="$(realpath .venv)"
    export XDG_CACHE_HOME="$(realpath .cache)"
    unset SOURCE_DATE_EPOCH
    test -d "$VENV" || (${python}/bin/python -m venv "$VENV"
      source "$VENV/bin/activate"
      pip install -Uc requirements/all.txt pip
      pip install -Uc requirements/all.txt cython
      pip install -Ur requirements/all.txt
      pip install -c requirements/all.txt --no-binary :all: --force ujson
      flit install -s
    )
    (
      IFS=:
      for x in $PYTHONPATH; do echo $x; done |sort -u > $VENV/${python.sitePackages}/nixpkgs.pth
    )
    source "$VENV/bin/activate"
    eval "$(_TRICE_COMPLETE=source trice)"
  '';

  buildInputs = [
    libffi
    python
  ];
}
