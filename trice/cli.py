# Copyright 2017 - 2020  Ternaris.
# SPDX-License-Identifier: Apache-2.0

"""Command-line interface to trice."""

import os
import re
from contextlib import asynccontextmanager
from pathlib import Path

import click
from jinja2 import Template
from yarl import URL

from . import gitlab
from .utils import asyncio_run, echo, launch_pdb_on_exception, pass_get


@asynccontextmanager
async def get_api(url: URL):
    async with gitlab.API(url=f'{url.scheme}://{url.host}', token=await pass_get(url.host)) as api:
        yield api


@click.group()
def trice():
    """Trace development artifacts with git."""


@trice.group('artifact')
def trice_artifact():
    """Manage artifacts."""


@trice_artifact.command('render')
@click.option('-t', '--target-directory', default='./artifacts', type=Path, show_default=True)
@click.argument('url', type=URL)
@asyncio_run
async def trice_artifact_render(target_directory, url):
    """Render item at URL to markdown.

    URL points to issue or merge request web url.
    """
    template = Template((Path(__file__).parent / 'template.jinja2').read_text())

    async with get_api(url) as api:
        dct = re.search(r'/(?P<project>.*?)/-/(?P<type>[^/]+)/(?P<iid>\d+)$', url.path).groupdict()
        endpoint = getattr(api.projects, dct['type'])
        item = await endpoint.getone(dct['project'], int(dct['iid']), model=True)
        notes = await endpoint.notes.getall(dct['project'], int(dct['iid']), model=True,
                                            params={'sort': 'asc'})

        target_directory.mkdir(exist_ok=True)
        itemdir = target_directory / dct['type']
        itemdir.mkdir(exist_ok=True)
        (itemdir / f"{dct['iid']}.md").write_text(template.render(item=item, notes=notes))


@trice.group('label')
def trice_label():
    """Manage GitLab labels."""


@trice_label.command('add')
@click.argument('project_url', type=URL)
@click.argument('name')
@click.argument('color')
@asyncio_run
async def trice_label_add(project_url, name, color):
    """Add new project label."""
    async with get_api(project_url) as api:
        label = await api.projects.labels.create(project_url.path.strip('/'),
                                                 data={'name': name, 'color': color},
                                                 model=True)
    echo(f'Created {label!r}')


@trice_label.command('ls')
@click.argument('project_url', type=URL)
@asyncio_run
async def trice_label_list(project_url):
    """List project labels."""
    async with get_api(project_url) as api:
        labels = await api.projects.labels.getall(project_url.path.strip('/'), model=True)
    for label in labels:
        echo(f'{label.name}: {label.upstream_id}')


@trice_label.command('rm')
@click.argument('project_url', type=URL)
@click.argument('label_id')
@asyncio_run
async def trice_label_remove(project_url, label_id):
    """Remove project label."""
    async with get_api(project_url) as api:
        await api.projects.labels.delete(project_url.path.strip('/'), label_id)
    echo('Deleted label.')


@trice_label.command('update')
@click.argument('project_url', type=URL)
@click.argument('label_id')
@click.option('--name', help='New name for label')
@click.option('--color', help='New color for label')
@asyncio_run
async def trice_label_update(project_url, label_id, name, color):
    """Update project label."""
    data = {}
    if name:
        data['new_name'] = name
    if color:
        data['color'] = color
    async with get_api(project_url) as api:
        label = await api.projects.labels.update(project_url.path.strip('/'), label_id,
                                                 data=data, model=True)
    echo(f'Updated {label!r}')


def cli():
    """Entry-point for cli."""
    with launch_pdb_on_exception(os.environ.get('PDB')):
        trice(auto_envvar_prefix='TRICE')  # pylint: disable=unexpected-keyword-arg
