# Copyright 2017 - 2020  Ternaris.
# SPDX-License-Identifier: Apache-2.0

"""Trace development artifacts with git."""

__version__ = '0.1.0'
