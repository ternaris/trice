# Copyright 2019 - 2020  Ternaris.
# SPDX-License-Identifier: Apache-2.0

import mock
import pytest
from mock import call

from trice.gitlab.api import API


async def test_url():
    assert API.projects.name == 'projects'  # descriptor works on class-level

    async with API(url='url') as api:
        assert api.url == 'url/api/v4'
        assert api.projects.url == 'url/api/v4/projects'
        assert api.projects.issues.url == 'url/api/v4/projects/{}/issues'
        assert api.projects.merge_requests.url == 'url/api/v4/projects/{}/merge_requests'

        with pytest.raises(ValueError):
            await api.projects.getone()

        with pytest.raises(ValueError):
            await api.projects.getone(None)

        with mock.patch.object(api.client, 'get') as get:
            await api.projects.getone(1)
            await api.projects.getone('foo/bar')
        assert call('url/api/v4/projects/1', params=None) in get.mock_calls
        assert call('url/api/v4/projects/foo%2Fbar', params=None) in get.mock_calls

        with pytest.raises(ValueError):
            await api.projects.issues.getone(1)

        with mock.patch.object(api.client, 'get') as get:
            await api.projects.issues.getone(1, 1)
            await api.projects.issues.getone('foo/bar', 1)
        assert call('url/api/v4/projects/1/issues/1', params=None) in get.mock_calls
        assert call('url/api/v4/projects/foo%2Fbar/issues/1', params=None) in get.mock_calls
