# Copyright 2019 - 2020  Ternaris.
# SPDX-License-Identifier: Apache-2.0

# pylint: disable=too-few-public-methods,no-self-argument,no-self-use,invalid-name

from __future__ import annotations

from datetime import datetime
from typing import Optional, Tuple

from pydantic import BaseModel, Field, HttpUrl, validator


TYPEMAP = {
    datetime: lambda x: x.isoformat(),
    HttpUrl: str,
}


class Model(BaseModel):
    class Config:
        allow_mutation = False

    @validator('*', pre=True)
    def empty_is_none(cls, v):
        if v == '':
            return None
        return v


class Resource(Model):
    gservice_id: Optional[int]
    upstream_id: int = Field(..., alias='id')


class Project(Resource):
    fullname: str = Field(..., alias='path_with_namespace')


class User(Resource):
    avatar_url: HttpUrl
    fullname: Optional[str] = Field(None, alias='name')
    state: str
    username: str
    web_url: HttpUrl


class Issue(Resource):
    gproject_id: Optional[int]
    gupdated_at: Optional[datetime]

    assignees: Tuple[User, ...]
    author: User
    closed_at: Optional[datetime]
    created_at: datetime
    description: Optional[str]
    iid: int
    labels: Tuple[str, ...]
    project_id: int
    state: str
    title: str
    updated_at: datetime
    web_url: HttpUrl

    @validator('author', 'assignees', pre=True, each_item=True)
    def set_user_gservice_id(cls, v, values):
        if v is not None and (gservice_id := values.get('gservice_id')):
            v['gservice_id'] = gservice_id
        return v


class MergeRequest(Resource):
    gproject_id: Optional[int]
    gupdated_at: Optional[datetime]

    assignees: Tuple[User, ...]
    author: User
    closed_at: Optional[datetime]
    created_at: datetime
    description: Optional[str]
    iid: int
    labels: Tuple[str, ...]
    merged_at: Optional[datetime]
    project_id: int
    state: str
    title: str
    updated_at: datetime
    web_url: HttpUrl

    @validator('author', 'assignees', pre=True, each_item=True)
    def set_user_gservice_id(cls, v, values):
        if v is not None and (gservice_id := values.get('gservice_id')):
            v['gservice_id'] = gservice_id
        return v


class Note(Resource):
    author: User
    body: str
    created_at: datetime
    updated_at: Optional[datetime]


class Label(Resource):
    color: str
    text_color: str
    name: str
    description: Optional[str]
    is_project_label: Optional[bool]
