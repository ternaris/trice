# Copyright 2019 - 2020  Ternaris.
# SPDX-License-Identifier: Apache-2.0

# pylint: disable=too-few-public-methods

from contextlib import asynccontextmanager
from datetime import datetime, timezone
from string import Formatter
from typing import Optional, Type
from urllib.parse import quote_plus

from aiohttp import ClientSession, TCPConnector
from ujson import dumps, loads

from .resources import Issue, Label, MergeRequest, Note, Project, Resource

MAP = {
    bool: lambda x: str(x).lower(),
    datetime: lambda x: x.isoformat(),
}


class EP:
    """Describe API endpoints."""

    def __init__(self, endpoint, name=None):
        self.endpoint = endpoint
        self.name = name or endpoint.__name__.lower()

    def __get__(self, parent, objtype):
        if parent is None:
            return self

        base = getattr(parent, 'oneurl', parent.url)
        client = getattr(parent, 'client', None)
        return self.endpoint(url=f'{base}/{self.name}', client=client, extrakw=parent.extrakw)


class Endpoint:
    def __init__(self, url: str, client: Optional[ClientSession] = None, extrakw=None):
        self.url = str(url)
        self.client = client
        self.extrakw = extrakw

    @asynccontextmanager
    async def delete_(self, url):
        async with self.client.delete(url) as resp:
            yield resp

    @asynccontextmanager
    async def get_(self, url, params=None):
        params = self._translate(params) if params else None
        async with self.client.get(url, params=params) as resp:
            yield resp

    @asynccontextmanager
    async def post_(self, url, json):
        async with self.client.post(url, json=json) as resp:
            yield resp

    @asynccontextmanager
    async def put_(self, url, json):
        async with self.client.put(url, json=json) as resp:
            yield resp

    @staticmethod
    def _translate(params):
        return {
            k: MAP.get(type(v), lambda x: x)(v)
            for k, v in params.items()
        }


class ResourceEP(Endpoint):
    MODEL = None  # type: Optional[Type[Resource]]

    @property
    def oneurl(self):
        return f'{self.url}/{{}}'

    async def create(self, *args, data, model=None):
        url = self._mkurl(self.url, args)
        if model is True:
            model = self.MODEL
        async with self.post_(url, json=data) as resp:
            data = await resp.json(loads=loads)
            if model:
                return model.parse_obj(data)
            return data

    async def delete(self, *args):
        url = self._mkurl(self.oneurl, args)
        async with self.delete_(url):
            pass

    async def update(self, *args, data, model=None):
        url = self._mkurl(self.oneurl, args)
        if model is True:
            model = self.MODEL
        async with self.put_(url, json=data) as resp:
            data = await resp.json(loads=loads)
            if model:
                return model.parse_obj(data)
            return data

    async def getone(self, *args, model=None, extrakw=None):
        url = self._mkurl(self.oneurl, args)
        extrakw = {**(self.extrakw or {}), **(extrakw or {})}
        if model is True:
            model = self.MODEL
        async with self.get_(url) as resp:
            data = await resp.json(loads=loads)
            if extrakw:
                data.update(extrakw)
            if model:
                return model.parse_obj(data)
            return data

    async def getpages(self, *args, params=None, model=None, extrakw=None):
        url = self._mkurl(self.url, args)
        extrakw = {**(self.extrakw or {}), **(extrakw or {})}
        if model is True:
            model = self.MODEL
        while url:
            async with self.get_(url, params=params) as resp:
                now = datetime.now(timezone.utc)
                page = await resp.json(loads=loads)
                # TODO: extrawk is broken in case model not used
                if model:
                    page = [model(**x, **extrakw, gupdated_at=now) for x in page]
                yield page
                url = resp.links.get('next', {}).get('url')
                params = None  # now encoded into next url

    async def getall(self, *args, params=None, model=None, extrakw=None):
        items = []
        async for page in self.getpages(*args, params=params, model=model, extrakw=extrakw):
            items.extend(page)
        return items

    @staticmethod
    def _mkurl(url, args):
        nargs = sum(bool(x) for x in Formatter().parse(url) if x[1] is not None)
        if len(args) != nargs:
            raise ValueError(f'Not enough args for {url}')
        if not all(isinstance((err := x), (str, int)) for x in args):
            raise ValueError(f'Invalid type: {err}')  # noqa: F821  # pylint: disable=undefined-variable
        return url.format(*[quote_plus(str(x)) for x in args])


class Labels(ResourceEP):
    MODEL = Label


class Notes(ResourceEP):
    MODEL = Note


class Issues(ResourceEP):
    MODEL = Issue
    notes = EP(Notes)


class MergeRequests(ResourceEP):
    MODEL = MergeRequest
    notes = EP(Notes)


class Projects(ResourceEP):
    MODEL = Project
    labels = EP(Labels)
    issues = EP(Issues)
    merge_requests = EP(MergeRequests, name='merge_requests')


class API(Endpoint):
    projects = EP(Projects)

    def __init__(self, url, token=None, limit_per_host=0, extrakw=None):
        client = ClientSession(
            headers={'PRIVATE-TOKEN': token} if token else None,
            connector=TCPConnector(limit_per_host=limit_per_host),
            json_serialize=dumps,
            raise_for_status=True,
        )
        super().__init__(url=f'{url}/api/v4', client=client, extrakw=extrakw)

    async def close(self):
        await self.client.close()

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc_value, traceback):
        await self.close()
