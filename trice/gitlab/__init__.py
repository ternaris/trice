# Copyright 2019 - 2020  Ternaris.
# SPDX-License-Identifier: Apache-2.0

"""Interact with GitLab."""

from .api import API
from .resources import Project

__all__ = (
    'API',
    'Project',
)
