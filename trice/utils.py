# Copyright 2017 - 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

import asyncio
import sys
from asyncio import create_subprocess_exec
from asyncio.subprocess import PIPE
from contextlib import contextmanager
from functools import update_wrapper


def asyncio_run(func):
    """Wrap async function to run synchronously.

    This is useful to implement click commands using async functions.
    """
    def sync_func(*args, **kw):
        try:
            return asyncio.run(func(*args, **kw))
        except asyncio.CancelledError:
            err('Stopped.', exit=1)
    return update_wrapper(sync_func, func)


def echo(*args, **kw):
    """Wrap print to let linter forbid print usage."""
    print(*args, **kw)  # noqa: T001


def err(*args, exit=None, **kw):
    """Print to stderr and optionally exit."""
    print(*args, **kw, flush=True, file=sys.stderr)  # noqa: T001
    if exit is not None:
        sys.exit(exit)


@contextmanager
def launch_pdb_on_exception(launch=True):
    """Return contextmanager launching pdb upon exception.

    Use like this, to toggle via env variable:

    with launch_pdb_on_exception(os.environ.get('PDB')):
        cli()
    """
    # pylint: disable=broad-except
    try:
        yield
    except Exception:  # noqa
        if launch:
            import pdb  # pylint: disable=import-outside-toplevel
            pdb.xpm()  # pylint: disable=no-member
        else:
            raise


async def pass_get(key, prompt=True):
    try:
        proc = await create_subprocess_exec('pass', key, stdout=PIPE)
    except FileNotFoundError:
        err('pass not found in PATH, please install https://www.passwordstore.org/', exit=1)

    secret = (await proc.stdout.read()).rstrip()
    await proc.wait()
    if proc.returncode != 0:
        if not prompt:
            sys.exit(proc.returncode)
        else:
            proc = await create_subprocess_exec('pass', 'insert', key)
            await proc.wait()
            if proc.returncode != 0:
                sys.exit(proc.returncode)

            proc = await create_subprocess_exec('pass', key, stdout=PIPE)
            secret = (await proc.stdout.read()).rstrip()
            await proc.wait()
            if proc.returncode != 0:
                sys.exit(proc.returncode)
    return secret.decode('utf-8')
