=====
Trice
=====

Trace development artifacts with git.


Development setup
=================

.. code-block::

   python3.8 -m venv .venv
   source .venv/bin/activate
   pip install -Uc requirements/all.txt pip
   pip install -Uc requirements/all.txt cython
   pip install -Ur requirements/all.txt
   flit install -s

OR

.. code-block::

   nix-shell


Add dependencies
================

- Edit pyproject.toml

.. code-block::

   ./requirements/generate-in-files
   ./pip-mcompile


Update dependencies
===================

.. code-block::

   ./requirements/generate-in-files
   ./pip-mcompile -P <package>   <- selected package
   ./pip-mcompile -U             <- all packages


Credits
=======

Initial development was funded by `Apex.AI <https://www.apex.ai>`_.
