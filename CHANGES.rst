.. _changelog:

Changelog
---------

.. _upcoming_changes:

Upcoming (unreleased)
^^^^^^^^^^^^^^^^^^^^^

Added
~~~~~

Changed
~~~~~~~

Deprecated
~~~~~~~~~~

Removed
~~~~~~~

Fixed
~~~~~

Security
~~~~~~~~
